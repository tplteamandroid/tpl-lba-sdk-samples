/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tpl.lba.sdk.sample;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.tpl.lba.sdk.LBAConfig;
import com.tpl.lba.sdk.LocationBasedAds;
import com.tpl.lba.sdk.models.IRequestPermission;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

public class MyFragmentActivity extends FragmentActivity {

    private TextView tvStatus;
    private Button btnStart;
    private Handler handler = new Handler(Looper.getMainLooper());
    /**
     * Runnable to just update the statuses of LocationListener and Worker on frontend.
     */
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            updateStatus();
            handler.postDelayed(this, 2000);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_fragment_activity);

        btnStart = findViewById(R.id.btn_start);
        Button btnStop = findViewById(R.id.btn_stop);
        tvStatus = findViewById(R.id.tv_status);

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setupAndStartLBA();

                handler.post(runnable);
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocationBasedAds.get(MyFragmentActivity.this).onDestroy();
            }
        });
    }

    /**
     * Setting up the configuration and starting the worker to pull campaigns from server.
     * You must need to set {@link LBAConfig#apiKey}.
     * You also should need to set {@link LBAConfig#requestPermissions} to true and to call
     * {@link LocationBasedAds#onRequestPermissionsResult(int, String[], int[], IRequestPermission)}
     * in {@link FragmentActivity#onRequestPermissionsResult(int, String[], int[])}, SDK will take
     * care to request required permissions and its results and you will have a callback
     * {@link IRequestPermission#onPermissionGranted()} mean user granted permission. You also need
     * to set {@link LBAConfig#applicationId} for further permission resolution.
     */
    private void setupAndStartLBA() {
        // TODO Paste your API Key here
        String API_KEY = "YOUR_API_KEY_HERE";
        LBAConfig lbaConfig = new LBAConfig.LBAConfigBuilder()
                .apiKey(API_KEY)
                .requestPermissions(true)
                .applicationId(BuildConfig.APPLICATION_ID)
                .build();
        LocationBasedAds.get(MyFragmentActivity.this).start(lbaConfig);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    protected void onResume() {
        super.onResume();

        handler.post(runnable);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        handler.removeCallbacks(runnable);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // LBA SDK needs this callback to get permission results upon request. SDK will take of
        // requesting permission and LocationSettings when necessary. You just need to call the method.
        LocationBasedAds.get(MyFragmentActivity.this).onRequestPermissionsResult(requestCode, permissions,
                grantResults, new IRequestPermission() {
                    @Override
                    public void onPermissionGranted() {
                        // On Permission Granted calling start button's on click
                        btnStart.callOnClick();
                    }
                });
    }

    private void updateStatus() {
        String status = "LBA SDK Worker: ";
        if (LocationBasedAds.get(this).isWorkerStarted()) {
            status += "\nStarted ";
        } else {
            status += "\nNot Started ";
        }
        // WORKER STATE
        if (LocationBasedAds.get(this).isWorkerRunning())
            status += "& Running\n\n";
        else
            status += "& Not Running\n\n";
        // LOCATION LISTENER STATUS
        status += "Location Listener: ";
        if (LocationBasedAds.get(this).isLocationListenerStarted()) {
            status += "\nRunning";
        } else {
            status += "\nNot Running";
        }
        tvStatus.setText(status);
    }
}
